from django.db import models
from django.urls import reverse

class Category(models.Model):
  name = models.CharField(max_length=100, unique=True)

  def get_api_url(self):
      return reverse("api_category", kwargs={"pk": self.id})

  def __str__(self) -> str:
      return self.name

class Brand(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_brand", kwargs={"pk": self.id})

    def __str__(self) -> str:
      return self.name

class Gear(models.Model):

  class SaleStatus(models.TextChoices):
    SOLD = 'Sold'
    AVAILABLE = 'Available'

  class ConditionChoices(models.TextChoices):
      NEW = 'New'
      RECONDITIONED = 'Reconditioned'
      OPEN_BOX = 'Open box (never used)'
      USED = 'Used (normal wear)'
      FOR_PARTS = 'For parts'
      OTHER = 'See description'

  seller = models.CharField(max_length=200)
  created_at = models.DateTimeField(auto_now=True, null=True)
  title = models.CharField(max_length=200)
  image = models.URLField()
  category = models.ForeignKey(
    Category,
    related_name='gear',
    on_delete=models.PROTECT,
    null=True
  )
  condition = models.CharField(
      max_length=25,
      choices=ConditionChoices.choices,
      default=ConditionChoices.USED
    )
  brand = models.ForeignKey(
    Brand,
    related_name='gear',
    on_delete=models.PROTECT,
    null=True
  )
  color = models.CharField(max_length=50)
  size = models.CharField(max_length=50)
  msrp = models.DecimalField(max_digits=6, decimal_places=2, null=True)
  description = models.TextField(null=True)
  price = models.DecimalField(max_digits=6, decimal_places=2)
  sale_status = models.CharField(
    max_length=25,
    choices=SaleStatus.choices,
    default=SaleStatus.AVAILABLE,
    null=True
  )

  def __str__(self) -> str:
      return self.title


