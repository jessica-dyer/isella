from pyexpat import model
from common.json import ModelEncoder

from .models import Category, Brand, Gear
class DecimalEncoder(ModelEncoder):
  model = Gear
  properties = [
    'msrp',
    'price'
  ]

class CategoryEncoder(ModelEncoder):
  model= Category
  properties = [
    'id',
    'name'
  ]

class BrandEncoder(ModelEncoder):
  model = Brand
  properties = [
    'id',
    'name'
  ]

class GearListEncoder(ModelEncoder):
  model = Gear
  properties = [
    'id',
    'title',
    'seller',
    'category',
    'brand',
    'image',
    'price',
  ]
  encoders = {
    "category": CategoryEncoder(),
    "brand": BrandEncoder(),
  }

class GearDetailEncoder(ModelEncoder):
  model = Gear
  properties = [
    'title',
    'seller',
    'category',
    'brand',
    'image',
    'condition',
    'color',
    'size',
    'msrp',
    'description',
    'sale_status',
    'price',
    'created_at',
  ]
  encoders = {
    "category": CategoryEncoder(),
    "brand": BrandEncoder(),
  }