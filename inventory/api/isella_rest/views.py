from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
  CategoryEncoder,
  BrandEncoder,
  GearListEncoder,
  GearDetailEncoder
)

from .models import Category, Brand, Gear

# CATEGORIES
@require_http_methods(["GET", "POST"])
def api_categories(request):
  if request.method == "GET":
    categories = Category.objects.all()
    return JsonResponse(
      {"categories": categories},
      encoder
      =CategoryEncoder
    )
  else:
    try:
      content = json.loads(request.body)
      category = Category.objects.create(**content)
      return JsonResponse(
        category,
        encoder=CategoryEncoder,
        safe=False,
      )
    except:
      response = JsonResponse(
        {"message": "Could not create the category"}
      )
      response.status_code = 400
      return response

# BRANDS
@require_http_methods(["GET", "POST"])
def api_brands(request):
  if request.method == "GET":
    brands = Brand.objects.all()
    return JsonResponse(
      {"brands": brands},
      encoder
      =BrandEncoder
    )
  else:
    try:
      content = json.loads(request.body)
      brand = Brand.objects.create(**content)
      return JsonResponse(
        brand,
        encoder=BrandEncoder,
        safe=False,
      )
    except:
      response = JsonResponse(
        {"message": "Could not create the brand"}
      )
      response.status_code = 400
      return response

# GEAR
@require_http_methods(["GET", "POST"])
def api_gear_items(request):
  if request.method == "GET":
    gear = Gear.objects.all()
    return JsonResponse(
      {"gear": gear},
      encoder=GearListEncoder
    )
  else:
    try:
      content = json.loads(request.body)
      category_id = content["category"]
      category = Category.objects.get(id=category_id)
      content["category"] = category
      brand_id = content["brand"]
      brand = Brand.objects.get(id=brand_id)
      content["brand"] = brand
      gear = Gear.objects.create(**content)
      return JsonResponse(
        gear,
        encoder = GearListEncoder,
        safe=False,
      )
    except:
      response = JsonResponse(
                {"message": "Could not create the item of gear"}
            )
      response.status_code = 400
      return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_gear_details(request, id):
  if request.method == "GET":
    try:
      gear = Gear.objects.get(id=id)
      return JsonResponse(
        gear,
        encoder=GearDetailEncoder,
        safe=False
      )
    except Gear.DoesNotExist:
      response = JsonResponse({"message": "Does not exist"})
      response.status_code = 404
      return response
  elif request.method == "DELETE":
    try:
      gear = Gear.objects.get(id=id)
      gear.delete()
      return JsonResponse(
        gear,
        encoder=GearDetailEncoder,
        safe=False
      )
    except Gear.DoesNotExist:
      response = JsonResponse({"message": "Does not exist"})
  else: # PUT
    try:
      content = json.loads(request.body)
      gear = Gear.objects.get(id=id)
      props = ['title', 'category', 'brand', 'image', 'condition',
      'color', 'size', 'msrp', 'description', 'price', 'sale_status']
      for prop in props:
        if prop in content:
          setattr(gear, prop, content[prop])
      gear.save()

      return JsonResponse(
        gear,
        encoder=GearDetailEncoder,
        safe=False
      )
    except Gear.DoesNotExist:
      response = JsonResponse({"message": "Does not exist"})
      response.status_code = 404
      return response

