from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import (
  api_categories,
  api_brands,
  api_gear_items,
  api_gear_details
)

urlpatterns = [
  path(
    "categories/",
    api_categories,
    name="api_categories"
  ),
  path(
    "brands/",
    api_brands,
    name="api_brands"
  ),
  path(
    "gear/",
    api_gear_items,
    name='api_gear_items'
  ),
  path(
    "gear/<int:id>/",
    api_gear_details,
    name='api_gear_details'
  )
]


if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)
