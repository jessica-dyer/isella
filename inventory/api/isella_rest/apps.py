from django.apps import AppConfig


class IsellaRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'isella_rest'
