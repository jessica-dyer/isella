import React from 'react'

function Scholarship() {
  return (
    < div className="row" >
      <div className="col-8">
        <div class="card my-5" >
          <img src="https://glaciers.us/glaciers.research.pdx.edu/sites/default/files/images/Isella_2005_1.tiff.preview.png" className="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">Gear Scholarships</h5>
            <p class="card-text">Here at Isella, we believe everyone should have the opportunity to pursue their outdoor goals. That's why we offer gear scholarships to folks twice a year. Stay tuned for upcoming scholarships!</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Scholarship