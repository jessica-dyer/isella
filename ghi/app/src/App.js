import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SaleItemsPage from './gear_components/SaleItems'
import CreateSalePostingForm from './sale_components/SaleForm'
import SaleItemDetail from './gear_components/SaleItemDetail'
import Scholarship from './scholarship_components/ScholarshipPage'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shop">
            <Route path='' element={<SaleItemsPage />}></Route>
            <Route path=":id" element={<SaleItemDetail />}></Route>
          </Route>
          <Route path="sell">
            <Route path='' element={<CreateSalePostingForm />}></Route>
          </Route>
          <Route path="scholarship">
            <Route path='' element={<Scholarship />}></Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App;
