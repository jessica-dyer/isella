import React from 'react'
import { useParams } from "react-router-dom";
import { useEffect, useState } from 'react'
import moment from 'moment'


const SaleItemDetail = () => {
  const { id } = useParams()
  const BASE_URL = 'http://localhost:8100/api/gear'
  const [itemTitle, setCurrentItemTitle] = useState([])
  const [itemDescription, setCurrentItemDescription] = useState([])
  const [itemImage, setCurrentItemImage] = useState([])
  const [itemPrice, setCurrentItemPrice] = useState([])
  const [itemMsrp, setCurrentItemMsrp] = useState([])
  const [itemCreatedAt, setCurrentCreatedAt] = useState([])
  const [itemCondition, setCurrentItemCondition] = useState([])

  useEffect(() => {
    const itemUrl = `${BASE_URL}/${id}/`
    fetch(itemUrl)
      .then(res => res.json())
      .then(data => {
        setCurrentItemTitle(data.title)
        setCurrentItemDescription(data.description)
        setCurrentItemImage(data.image)
        setCurrentItemPrice(data.price)
        setCurrentItemMsrp(data.msrp)
        setCurrentCreatedAt(moment(data.created_at).fromNow())
        setCurrentItemCondition(data.condition)
      })
  }, [id])



  return (
    < div className="row" >
      <div className="col-sm-7">
        <div className="card my-5">
          <img src={itemImage} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5>Description</h5>
            <p className="card-text">{itemDescription}</p>
          </div>
        </div>
      </div>
      <div className="col-sm-5">
        <div className="card my-5">
          <div className="card-body">
            <h5 className="card-title">{itemTitle}</h5>
            <h4 className="card-text">${itemPrice}</h4>
            <p class="card-text"><small class="text-muted">Posted {itemCreatedAt}</small></p>
            <p>Condition: {itemCondition}</p>
            <p>Retail price: ${itemMsrp}</p>
            <div class="d-grid gap-2">
              <button class="btn btn-info" type="button">Make offer</button>
              <button class="btn btn-outline-info" type="button">Ask</button>
              <div className="row">
                <div className="col-8 my-3 py-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="purple" class="bi bi-heart-fill" viewBox="0 0 20 20">
                    <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
                  </svg>
                  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="purple" class="bi bi-share" viewBox="0 0 20 20">
                    <path d="M13.5 1a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM11 2.5a2.5 2.5 0 1 1 .603 1.628l-6.718 3.12a2.499 2.499 0 0 1 0 1.504l6.718 3.12a2.5 2.5 0 1 1-.488.876l-6.718-3.12a2.5 2.5 0 1 1 0-3.256l6.718-3.12A2.5 2.5 0 0 1 11 2.5zm-8.5 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm11 5.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3z" />
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div >
  )
}

export default SaleItemDetail