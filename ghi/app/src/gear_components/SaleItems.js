import React from 'react';
import { Link } from 'react-router-dom';

function SaleItemsCard(props) {

  return (
    <>
      <div className="card col-3 my-2 mx-2" style={{ width: 150 }}>
        <Link to={`/shop/${props.item.id}`} style={{ textDecoration: 'none', color: 'black' }}
          key={props.item.id}>
          <img src={props.item.image} className="card-img-top" alt="" width='200' />
          <h6 className="card-text">{props.item.title}</h6>
          <p>${props.item.price}</p>
        </Link>
      </div>
    </>
  )
}

class SaleItemsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      categories: [],
    };
  }
  async componentDidMount() {
    const url = 'http://localhost:8100/api/gear/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ items: data.gear })
    }

    const catUrl = 'http://localhost:8100/api/categories/';
    const catResponse = await fetch(catUrl);
    if (catResponse.ok) {
      const catData = await catResponse.json();
      this.setState({ categories: catData.categories })
    }
  }

  render() {
    return (
      <>
        <div className="row justify-content-center mt-3">
          <form className="col-6">
            <div className="input-group mb-3 mt-3">
              <input
                type="text"
                className="form-control"
                placeholder="Search"
                aria-label="search"
                aria-describedby="basic-addon2"
              />
              <div className="input-group-append">
                <button className="btn btn-outline-secondary" type="submit">
                  Search
                </button>
              </div>
            </div>
          </form>
        </div>

        <div className="px-4 py-3 my-1 text-center">
          <div className="col-sm-12 mx-auto">
            <div className="row justify-content-center">
              {this.state.items.map(item =>
                <SaleItemsCard
                  key={item.id}
                  item={item}
                ></SaleItemsCard>)}
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default SaleItemsPage