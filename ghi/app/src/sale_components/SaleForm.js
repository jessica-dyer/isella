import React from 'react';

class CreateSalePostingForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      seller: '',
      title: '',
      image: '',
      category: '',
      condition: '',
      color: '',
      brand: '',
      size: '',
      msrp: '',
      description: '',
      price: '',
      categories: [],
      brands: [],
      conditions: ['New', 'Reconditioned', 'Open box (never used)', 'Used (normal wear)', 'For parts', 'See description'],
    }
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handleTitleChange = this.handleTitleChange.bind(this)
    this.handleImageChange = this.handleImageChange.bind(this)
    this.handleCategorySelection = this.handleCategorySelection.bind(this)
    this.handleConditionSelection = this.handleConditionSelection.bind(this)
    this.handleColorChange = this.handleColorChange.bind(this)
    this.handleBrandSelection = this.handleBrandSelection.bind(this)
    this.handleSizeChange = this.handleSizeChange.bind(this)
    this.handleMsrpChange = this.handleMsrpChange.bind(this)
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
    this.handlePriceChange = this.handlePriceChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleEmailChange(e) {
    const value = e.target.value;
    this.setState({ seller: value })
  }

  handleTitleChange(e) {
    const value = e.target.value;
    this.setState({ title: value })
  }

  handleImageChange(e) {
    const value = e.target.value;
    this.setState({ image: value })
  }

  handleCategorySelection(e) {
    const value = e.target.value;
    this.setState({ category: value })
  }

  handleConditionSelection(e) {
    const value = e.target.value;
    this.setState({ condition: value })
  }

  handleColorChange(e) {
    const value = e.target.value;
    this.setState({ color: value })
  }

  handleBrandSelection(e) {
    const value = e.target.value;
    this.setState({ brand: value })
  }

  handleSizeChange(e) {
    const value = e.target.value;
    this.setState({ size: value })
  }

  handleMsrpChange(e) {
    const value = e.target.value;
    this.setState({ msrp: value })
  }

  handleDescriptionChange(e) {
    const value = e.target.value;
    this.setState({ description: value })
  }

  handlePriceChange(e) {
    const value = e.target.value;
    this.setState({ price: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    console.log({ ...this.state })
    const data = { ...this.state };
    delete data.brands;
    delete data.categories;
    delete data.conditions;
    console.log(data)

    const salePostUrl = 'http://localhost:8100/api/gear/'
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'content-Type': 'application/json',
      },
    };
    const response = await fetch(salePostUrl, fetchConfig)
    if (response.ok) {
      const cleared = {
        seller: '',
        title: '',
        image: '',
        category: '',
        condition: '',
        color: '',
        brand: '',
        size: '',
        msrp: '',
        description: '',
        price: '',
        categories: [],
        brands: [],
        conditions: ['New', 'Reconditioned', 'Open box (never used)', 'Used (normal wear)', 'For parts', 'See description']
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const categoryUrl = 'http://localhost:8100/api/categories/'
    const categoryResponse = await fetch(categoryUrl);

    if (categoryResponse.ok) {
      const categoryData = await categoryResponse.json();
      this.setState({ categories: categoryData.categories })
    }

    const brandUrl = 'http://localhost:8100/api/brands/'
    const brandResponse = await fetch(brandUrl);
    if (brandResponse.ok) {
      const brandData = await brandResponse.json();
      this.setState({ brands: brandData.brands })
    }

  }

  render() {
    let formClasses = '';
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Post an item for sale</h1>
            <form className={formClasses} onSubmit={this.handleSubmit} id="create-sale-posting-form" >
              <div className="form-floating mb-3">
                <input onChange={this.handleEmailChange} value={this.state.seller} placeholder="Seller e-mail address" required type="email" name="email" id="email" className="form-control" />
                <label htmlFor="email">Seller e-mail</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleImageChange} value={this.state.image} placeholder="Image URL" required type="text" name="image" id="image" className="form-control" />
                <label htmlFor="image">Image</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleCategorySelection} value={this.state.category} required name="category" id="category" className="form-select">
                  <option value="">Choose a category</option>
                  {this.state.categories.map(category => {
                    return (
                      <option key={category.id} value={category.id}>
                        {category.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="mb-3">
                <select onChange={this.handleConditionSelection} value={this.state.condition} required name="condition" id="condition" className="form-select">
                  <option value="">Condition</option>
                  {this.state.conditions.map(condition => {
                    return (
                      <option key={condition} value={condition}>
                        {condition}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='form-floating mb-3'>
                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleBrandSelection} value={this.state.brand} required name="brand" id="brand" className="form-select">
                  <option value="">Brand</option>
                  {this.state.brands.map(brand => {
                    return (
                      <option key={brand.id} value={brand.id}>
                        {brand.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className='form-floating mb-3'>
                <input onChange={this.handleSizeChange} value={this.state.size} placeholder="Size" required type="text" name="size" id="size" className="form-control" />
                <label htmlFor="size">Size</label>
                <div id="sizeHelp" className="form-text">For example: XXS, XS, S, M, L, XL, 2XL, 3XL, 4XL, 5XL, 6XL, 7XL, 8XL, 9XL, 10XL
                </div>
              </div>
              <div className='form-floating mb-3'>
                <input onChange={this.handleMsrpChange} value={this.state.msrp} placeholder="MSRP" required type="number" name="msrp" id="msrp" className="form-control" />
                <label htmlFor="msrp">MSRP</label>
                <div id="msrpHelp" className="form-text">MSRP: Manufacturer's Suggested Retail Price
                </div>
              </div>
              <div className='form-floating mb-3'>
                <textarea onChange={this.handleDescriptionChange} value={this.state.description} placeholder="Description" required type="textarea" name="description" id="description" className="form-control" />
                <label htmlFor="description">Description</label>
              </div>
              <div className='form-floating mb-3'>
                <input onChange={this.handlePriceChange} value={this.state.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div >
    )
  }
}

export default CreateSalePostingForm