import { NavLink } from 'react-router-dom';
import logo from './Isella-logo-purple-hat-small.png'

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg" style={{ "backgroundColor": "#312054" }}>
      <div className="container-fluid">
        {/* <img src={logo} alt="" width='100' /> */}
        <NavLink className="navbar-brand" to="/"><img src={logo} alt="" width='100' /></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {/* <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="" style={({ isActive }) => {
                return {
                  color: isActive ? "purple" : "",
                };
              }} >Home</NavLink>
            </li> */}
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="shop" style={({ isActive }) => {
                return {
                  color: isActive ? "white" : "",
                };
              }} >Shop</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="sell" style={({ isActive }) => {
                return {
                  color: isActive ? "white" : "",
                };
              }} >Sell</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="scholarship" style={({ isActive }) => {
                return {
                  color: isActive ? "white" : "",
                };
              }} >Gear Scholarships</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
