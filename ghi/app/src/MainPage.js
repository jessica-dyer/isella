import logo from './Isella-logo-purple-hat-small.png'


function MainPage() {
  return (
    <>
      <div className="px-4 py-5 my-5 text-center">

        {/* <h1 className="display-5 fw-bold">Isella</h1> */}
        <img src={logo} alt='' width='400' />
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The premiere solution for outdoor gear consignment!
          </p>
          {/* <div class="card">
            <img src="https://glaciers.us/glaciers.research.pdx.edu/sites/default/files/images/Isella_2005_1.tiff.preview.png" className="card-img-top" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Isella Outdoor Consignment</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
          </div> */}
        </div>
      </div>
    </>
  );
}

export default MainPage;