import { Link } from 'react-router-dom';

export default function NavButton(props) {
  const {
    link,
    buttonText
  } = props
  return (
    <>
      <Link to={`${link}`} style={{ textDecoration: 'none' }}>
        <div className="d-grid p-1 tm-10">
          <button className="btn btn-outline-dark btn-lg px-4 gap-3" type="button">{buttonText}</button>
        </div>
      </Link>
    </>
  )
}